import './Dashboard.css';

import { useState } from 'react';

import Footer from './components/Footer';
import GridRow from '../../components/GridRow';
import Header from './components/Header';
import Modal from '../../components/Modal';

import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';


const initialDeliveries = [
  {
    status: 'ready',
    orderId: '009-300CFT',
    technician: 'Ben Sanatana',
    platform: 'Gamma',
    drone: 'DJI-004Q',
    technichalCheck: 'Passsed'
  },
  {
    status: 'ready',
    orderId: '008-300CFT',
    technician: 'Ben Sanatana',
    platform: 'Gamma',
    drone: 'DJI-004Q',
    technichalCheck: 'Passsed'
  },
  {
    status: 'ready',
    orderId: '007-300CFT',
    technician: 'Ben Sanatana',
    platform: 'Gamma',
    drone: 'DJI-004Q',
    technichalCheck: 'Passsed'
  },
  {
    status: 'ready',
    orderId: '006-300CFT',
    technician: 'Ben Sanatana',
    platform: 'Gamma',
    drone: 'DJI-004Q',
    technichalCheck: 'Passsed'
  },
  {
    status: 'ready',
    orderId: '005-300CFT',
    technician: 'Ben Sanatana',
    platform: 'Gamma',
    drone: 'DJI-004Q',
    technichalCheck: 'Passsed'
  },
  {
    status: 'ready',
    orderId: '004-300CFT',
    technician: 'Ben Sanatana',
    platform: 'Gamma',
    drone: 'DJI-004Q',
    technichalCheck: 'Passsed'
  },
  {
    status: 'ready',
    orderId: '003-300CFT',
    technician: 'Ben Sanatana',
    platform: 'Gamma',
    drone: 'DJI-004Q',
    technichalCheck: 'Passsed'
  },
  {
    status: 'ready',
    orderId: '002-300CFT',
    technician: 'Ben Sanatana',
    platform: 'Gamma',
    drone: 'DJI-004Q',
    technichalCheck: 'Passsed'
  },
  {
    status: 'ready',
    orderId: '001-300CFT',
    technician: 'Ben Sanatana',
    platform: 'Gamma',
    drone: 'DJI-004Q',
    technichalCheck: 'Passsed'
  },
]

function Dashboard() {
  const [deliveries, setDeliveries] = useState(initialDeliveries);
  const [show, setShow] = useState(false);
  const [newOrder, setNewOrder] = useState({
    orderId: '',
    technician: '',
    platform: '',
    drone: '',
  });

  console.log('newOrder', newOrder);

  const handleNewOrder = (key, value) => {
    setNewOrder(prevState => ({ ...prevState, [key]: value }));
  };

  const createNewOrder = () => {
    setDeliveries(prevState => ([...prevState, {...newOrder, status: 'pending', technichalCheck: 'Waiting'}]));
    setShow(false);
  }

  return (
    <>
      <div className="dashboardContainer">
        <Header handleNewDelivery={() => setShow(true)}/>
        {
          deliveries.map(item => (
            <GridRow
              key={item.orderId}
              status={item.status}
              orderId={item.orderId}
              technician={item.technician}
              platform={item.platform}
              drone={item.drone}
              technichalCheck={item.technichalCheck}
            />
          ))
        }
        <Footer />
      </div>
      <Modal title="New Delivery" show={show} onClose={() => setShow(false)} onSuccess={createNewOrder}>
        <p style={{ display: 'inline' }}>Please select the orderId and a technician to deploy the cargo.</p>
        <p style={{ display: 'inline' }}>All elements are mandatory</p>
        <div className="modalFormContainer" style={{ marginBottom: '1em'}}>
        <FormControl sx={{ minWidth: 220 }}>
          <TextField
            id="orderId"
            label="Order ID"
            variant="standard"
            onChange={(e) => handleNewOrder('orderId', e.target.value)}
          />
        </FormControl>
        <FormControl sx={{ minWidth: 220 }}>
          <TextField
            id="technician"
            label="Technician"
            variant="standard"
            onChange={(e) => handleNewOrder('technician', e.target.value)}
          />
        </FormControl>
        </div>
        <div style={{ display: 'flex', justifyContent: 'space-around' }}>
          <FormControl sx={{ minWidth: 220 }}>
            <InputLabel id="platform">Platform</InputLabel>
            <Select
              labelId="platform"
              id="platform"
              value={newOrder.platform}
              label="Platform"
              onChange={(e) => handleNewOrder('platform', e.target.value)}
            >
              <MenuItem value="Alpha">Alpha</MenuItem>
              <MenuItem value="Gamma">Gamma</MenuItem>
              <MenuItem value="Beta">Beta</MenuItem>
            </Select>
            </FormControl>
            <FormControl sx={{ minWidth: 220 }}>
            <InputLabel id="drone">Drone</InputLabel>
            <Select
              labelId="drone"
              id="drone"
              value={newOrder.drone}
              label="Drone"
              onChange={(e) => handleNewOrder('drone', e.target.value)}
            >
              <MenuItem value="DJI-004Q">DJI-004Q</MenuItem>
              <MenuItem value="DJI-005Q">DJI-005Q</MenuItem>
              <MenuItem value="DJI-006Q">DJI-006Q</MenuItem>
            </Select>
            </FormControl>
          </div>
      </Modal>
    </>
  );
}

export default Dashboard;
