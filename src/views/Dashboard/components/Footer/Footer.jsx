import './Footer.css';

function Footer() {
  return (
      <div className="container" style={{ marginBottom: '1em'}}>
        <div><p>Powered by Nuvo Cargo</p></div>
        <div><p>Help</p></div>
      </div>
  );
}

export default Footer;
