import './Header.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleUser } from '@fortawesome/free-solid-svg-icons';

import Button from '../../../../components/Button';

function Header({handleNewDelivery}) {
  return (
    <>
      <div className="container" style={{ marginBottom: '1em'}}>
        <div><p>DronoCargo</p></div>
        <div>
          <p>
            Jonatan Camarena
            <FontAwesomeIcon style={{ paddingLeft: '1em'}} icon={faCircleUser} />
          </p>
        </div>
      </div>
      <div className="container">
        <div style={{ fontSize: '1.5em' }}>
          <p style={{ display: 'inline' }}>Delivery</p>
          <p style={{ display: 'inline', paddingLeft: '1em', color: 'grey'}}>History</p>
        </div>
        <div>
          <Button handleOnClick={handleNewDelivery}>New Delivery</Button>
        </div>
      </div>
    </>
  );
}

export default Header;
