import { Routes, Route, useParams } from "react-router-dom";


function Details() {
  let params = useParams();

  return (
      <div>
        <div><p>Details:</p></div>
        <div><p>{params.orderID}</p></div>
      </div>
  );
}

export default Details;
