import './GridRow.css';

import { useNavigate } from "react-router-dom";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faChevronDown } from '@fortawesome/free-solid-svg-icons';

import Button from '../Button'

const GridRow = (props) => {
    const {
        status,
        orderId,
        technician,
        platform,
        drone,
        technichalCheck,
    } = props;

    let navigate = useNavigate();

    return (
        <div className="row">
            <div className="column">
                <p className="title">Status</p>
                <p className="subtitle" >{status}</p>
            </div>
            <div className="column">
                <p className="title">Order ID</p>
                <p className="subtitle">{orderId}</p>
            </div>
            <div className="column">
                <p className="title">Technician</p>
                <p className="subtitle">{technician}</p>
            </div>
            <div className="column">
                <p className="title">Platform</p>
                <p className="subtitle">{platform}</p>
            </div>
            <div className="column">
                <p className="title">Drone</p>
                <p className="subtitle">{drone}</p>
            </div>
            <div className="column">
                <p className="title">Technichal Check</p>
                <p className="subtitle">{technichalCheck}</p>
            </div>
            <div className="column">
                <p className="title">Actions</p>
                <div style={{  display: 'flex', justifyContent: 'space-around' }}>
                    <Button type="outline" handleOnClick={() => {navigate(`/orderDetails/${orderId}`);}}>
                        <div>
                            <p style={{ display: 'inline' }}>Details</p>
                            <FontAwesomeIcon style={{ color: 'lightgrey', paddingLeft: '1em'}} icon={faBars} />
                        </div>
                    </Button>
                    <Button type="outline">
                        <div>
                            <p style={{ display: 'inline' }}>Actions</p>
                            <FontAwesomeIcon style={{ color: 'lightgrey', paddingLeft: '1em'}} icon={faChevronDown} />
                        </div>
                    </Button>
                </div>
            </div>
        </div>
  )
};

export default GridRow;