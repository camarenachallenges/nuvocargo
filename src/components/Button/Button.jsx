import './Button.css';

import classnames from 'classnames';

const Button = (props) => {
    const {
        children,
        handleOnClick = () => {},
        type = 'default',
        styles = {}
    } = props;

        return (
            <button style={{...styles}} className={classnames([type === 'outline' ? 'outlineButton' : 'button'])} onClick={handleOnClick}>
                {children}
            </button>
        );
}

export default Button; 