import './Modal.css';

import Button from '../Button';

import classnames from 'classnames';

const Modal = (props) => {
    const {
        title,
        children,
        show,
        onClose,
        onSuccess
    } = props;

    return (
        <div className={classnames(['modal', show && 'show'])}  onClick={onClose}>
        <div className="modal-content" onClick={e => e.stopPropagation()}>
          <div className="modal-header">
            <h4 className="modal-title">{title}</h4>
          </div>
          <div className="modal-body">{children}</div>
          <div className="modal-footer">
            <div style={{  display: 'flex', justifyContent: 'flex-end' }}>
              <Button type="outline" handleOnClick={onClose}>
                  <div>
                      <p style={{ display: 'inline' }}>Cancel</p>
                  </div>
              </Button>
              <Button handleOnClick={onSuccess} styles={{  marginLeft: '1em' }} >
                  <div>
                      <p style={{ display: 'inline' }}>Create new delivery</p>
                  </div>
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
};

export default Modal;